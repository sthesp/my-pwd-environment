FROM alpine/git:latest

COPY ./entrypoint.sh /entrypoint.sh

RUN chmod +x /entrypoint.sh

RUN cd / && git clone https://github.com/Starli0n/dotfiles
