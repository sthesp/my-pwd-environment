.PHONY: config
config:
	docker-compose -f pwd-stack.yml config

.PHONY: build
build:
	docker build -t my-pwd-environment:latest .

.PHONY: up
up:
	docker-compose -f pwd-stack.yml up --build -d

.PHONY: down
down:
	docker-compose -f pwd-stack.yml down

.PHONY: shell
shell:
	docker run -it --rm --entrypoint /bin/sh my-pwd-environment:latest
